function requireAll(r) {
  r.keys().forEach(r);
}
requireAll(require.context('../svg/', true, /\.svg$/));

// we can just hit CRTL + S to trigger rebundle when dev server is active to get new icons added